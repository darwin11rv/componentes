
# Proyecto de componentes ionic4
## Descripción
Este proyecto de prueba con el framwork ionic 4, el objetivo es de generar una biblioteca de ocnsulta de cada elemeto o componente que permite utilizar el framawowrk Ionic4.
Paquete | Version
------------------|---------------------------------------
ionic (Ionic CLI) | 4.12.0 (/usr/lib/node_modules/ionic)
Ionic Framework | @ionic/angular 4.2.0
@angular-devkit/build-angular | 0.13.8
@angular-devkit/schematics | 7.2.4
@angular/cli | 7.3.8
@ionic/angular-toolkit | 1.4.1

Con las versiones detalladas el componente date time en value registra un valor menor al indicado ej:

```
    <ion-item>
      <ion-label>Años</ion-label>
    
      <ion-datetime display-format="YYYY" aria-placeholder="yyyy" min="2015" max="2018" value="2016">

      </ion-datetime>
    </ion-item>

```
   * `El resultado es 2015`