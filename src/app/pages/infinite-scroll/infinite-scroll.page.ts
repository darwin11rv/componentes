import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-infinite-scroll',
  templateUrl: './infinite-scroll.page.html',
  styleUrls: ['./infinite-scroll.page.scss'],
})
export class InfiniteScrollPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll:IonInfiniteScroll;

  data:any[] = Array(20);
  constructor() { }

  ngOnInit() {
  }

  loadData(event){
    console.log("cargar siguiente");
    ///simulando una carga asincrona
    setTimeout(() => {
      const nuevoArr =Array(20);
      this.data.push(...nuevoArr);
      console.log("todos los datos cargados correctamente");
      event.target.complete();
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.data.length > 40 ) {

        /** otra forma de deshabilitar el infinity scroll
         * 
         * this.infiniteScroll.disabled = true;
         * return;
         */
        event.target.disabled = true;
      }
    }, 1000);
  }

}
