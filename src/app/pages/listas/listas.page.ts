import { Component, OnInit, ViewChild } from '@angular/core';
import { PlaceholderService } from '../../services/placeholder.service';
import { Observable } from 'rxjs';
import { IonList } from '@ionic/angular';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.page.html',
  styleUrls: ['./listas.page.scss'],
})
export class ListasPage implements OnInit {

  private url: string = "https://jsonplaceholder.typicode.com/users";
  usuarios: Observable<any>;

  @ViewChild('listaus') public lista: IonList;

  constructor(private usersSer: PlaceholderService) { }

  ngOnInit() {
    this.usuarios = this.usersSer.getUsers(this.url);
  }

  favorite( us) {
    console.log("favorito :",us)
    this.lista.closeSlidingItems();
  }
  share(us) {
    console.log("share :",us)
    this.lista.closeSlidingItems();
  }
  unread(us) {
    console.log("unread :",us)
    this.lista.closeSlidingItems();
  }


}
