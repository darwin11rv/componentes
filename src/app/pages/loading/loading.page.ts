import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit {

  constructor(public loadingCtrl:LoadingController) { }
  loading:any;
  ngOnInit() {
    this.presentLoadingWithOptions("espere");
    setTimeout(() => {
      this.loading.dismiss();
    }, 1500);
  }

  async presentLoadingWithOptions(mensaje:string) {
    this.loading = await this.loadingCtrl.create({
      spinner: null,
      //duration: 5000,
      message: mensaje,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await this.loading.present();
  }

}
