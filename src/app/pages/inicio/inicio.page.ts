import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Componente } from '../../Interfaces/interfaces';
import { Observable } from 'rxjs';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  componentes: Observable<Componente[]>;

  constructor(private menuCrtl: MenuController, private menuSer: MenuService) { }

  ngOnInit() {
    this.componentes = this.menuSer.getMenuOpt();
  }
  toggleMenu() {
    this.menuCrtl.toggle();
  }

}

