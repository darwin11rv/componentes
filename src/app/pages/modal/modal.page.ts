import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalInfoPage } from '../modal-info/modal-info.page';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {



  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async abriModal() {
    const modal= await this.modalCtrl.create({
      component: ModalInfoPage,
      componentProps: {
        'nombre': 'Darwin',
        'pais': 'Ecuador'
      }
    });
    await modal.present();
    //// aqui se puede tener el codigo para recibir parametros del modal modal info

    // esta es una promesa que escucha cuando el modal se cierra y guarda lo que venga en el dissmis en la variable data
    const { data } = await modal.onDidDismiss();
    console.log("retorno ", data)
    
  }
}
