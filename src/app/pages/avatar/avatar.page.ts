import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.page.html',
  styleUrls: ['./avatar.page.scss'],
})
export class AvatarPage implements OnInit {

  avatars: persona[] = [
    {
      nombre: "persona1",
      icon: "av1.png"
    },
    {
      nombre: "persona2",
      icon: "av2.png"
    },
    {
      nombre: "persona3",
      icon: "av3.png"
    },
    {
      nombre: "persona4",
      icon: "av4.png"
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}

interface persona {
  nombre: string,
  icon: string
}