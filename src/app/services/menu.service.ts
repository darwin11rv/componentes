import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Componente } from '../Interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor( private http:HttpClient) { }

  getMenuOpt(){
    return this.http.get<Componente[]>("/assets/data/menu.json");
  }

}
