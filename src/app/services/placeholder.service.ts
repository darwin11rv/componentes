import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {
  //url:"https://jsonplaceholder.typicode.com/users";
  constructor(private http:HttpClient) { }


  getUsers(url:string){
    return this.http.get(url);
  }

}
